# shop
开发还没有完成，暂告一段落

#### 介绍
前后端分离
后台管理前端页面请看仓库 [shop_HTML](https://gitee.com/MarkPolaris/shop_HTML)<br>
本项目具体内容介绍请看 [wiki](https://gitee.com/MarkPolaris/shop/wikis)

#### 软件架构
Spring,SpringMVC,Mybatis<br>
通用mapper
采用Restful风格

#### 参与贡献

1. mark(本人)
2. qiang


#### 其他

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 个人博客 [博客园](https://www.cnblogs.com/MC-Curry/)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)  
具体过程请看 [wiki](https://gitee.com/MarkPolaris/shop/wikis)