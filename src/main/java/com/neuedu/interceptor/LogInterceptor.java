package com.neuedu.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: MARK
 * @Date: 2019/8/2 08:09
 * @Version: 1.0.0
 * @Description: 拦截器
 */

@Slf4j
public class LogInterceptor implements HandlerInterceptor {

    /**
     * @return boolean
     * @create: 2019/8/2
     * @author MARK
     * @Description: controller执行前调用此方法
     * 返回true继续执行
     * 这里可以加入登录校验，权限拦截等
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // String remoteAddress = request.getHeader("Remote Address");
        //请求方式
        String RequestMethod = request.getMethod();
        //user-agent
        String userAgent = request.getHeader("User-Agent");
        //Referer
        String referer = request.getHeader("Referer");
        //ip
        String realIp = getIpAddress(request);
        //ip
        String host = request.getRemoteHost();
        //port
        int port = request.getRemotePort();
        //协议
        String scheme = request.getScheme();
        // log.info("------remoteAddress: " + remoteAddress);
        log.info(
                "------拦截器拦截信息------\n" +
                "------RequestMethod: " + RequestMethod + "\n" +
                "------userAgent: " + userAgent + "\n" +
                "------referer: " + referer + "\n" +
                "------realIp: " + realIp + "\n" +
                "------host: " + host + "\n" +
                "------port: " + port + "\n" +
                "------scheme: " + scheme + "\n" +
                "------getSession: " + request.getSession() + "\n" +
                "------RequestMethod: " + RequestMethod + "\n"

        );
        return true;
    }


    /**
     * @return void
     * @create: 2019/8/2
     * @author MARK
     * @Description: controller执行后但未返回视图前调用此方法
     */
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        log.info("postHandle");
    }

    /**
     * @return void
     * @create: 2019/8/2
     * @author MARK
     * @Description: controller执行后且视图返回后调用此方法
     */
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        log.info("afterHandle");
    }

    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
}
