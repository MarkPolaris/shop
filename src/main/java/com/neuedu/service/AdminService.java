package com.neuedu.service;

import com.neuedu.entity.Admin;

import java.util.List;

public interface AdminService {
    List<Admin> getAll();

    Admin getById(Integer id);

    void add(Admin admin);

    void update(Admin admin);

    void delete(Integer id);

    void batchDelete(List<Integer> ids);
}
