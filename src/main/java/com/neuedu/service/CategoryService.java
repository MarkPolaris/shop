package com.neuedu.service;

import com.neuedu.entity.Category;

import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/5 18:21
 * @Version: 1.0.0
 * @Description:
 */
public interface CategoryService {
    void add(Category category);

    void deleteById(Integer id);

    void update(Category category);

    List<Category> selectAll();

    Category findById(Integer id);

    // void batchDelete(List<Integer> ids);

    List<Category> findByPid(Integer pid);

    List<Integer> getGradeIds();

    List<Category> findByGrade(Integer grade);
}
