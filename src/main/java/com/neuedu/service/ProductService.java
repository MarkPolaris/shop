package com.neuedu.service;

import com.neuedu.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAll();

    Product getById(Integer id);

    void insert(Product product);

    void update(Product product);

    void delete(Integer id);

    void bathDelete(List<Integer> ids);
}
