package com.neuedu.service;

import com.neuedu.entity.SalesItem;
import com.neuedu.vo.SalesItemVO;

import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 16:28
 * @Version: 1.0.0
 * @Description:
 */
public interface SalesItemService {
    void add(SalesItem salesItem);

    void deleteById(Integer id);

    void batchDelete(List<Integer>ids);

    void deleteByOrderId(List<Integer> orderIds);

    void update(SalesItem salesItem);

    List<SalesItemVO> findAll();

    List<SalesItemVO> findAllBySalesOrderId(Integer salesOrderId);

    SalesItemVO findById(Integer id);

    List<Integer> findByOrderId(Integer orderId);

}
