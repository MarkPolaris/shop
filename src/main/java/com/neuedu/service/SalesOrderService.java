package com.neuedu.service;

import com.neuedu.entity.SalesOrder;
import com.neuedu.mapper.SalesOrderMapper;
import com.neuedu.vo.SalesOrderVO;

import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 19:46
 * @Version: 1.0.0
 * @Description:
 */
public interface SalesOrderService {
    void add(SalesOrder salesOrder);

    void delete(Integer salesOrderId);

    void deleteById(Integer id);

    void deleteByUserId(List<Integer> userIds);

    void update(SalesOrder salesOrder);

    List<SalesOrderVO> findAll();

    List<Integer> orderIds(List<Integer> userIds);

    SalesOrderVO findBySalesOrderId(Integer salesOrderId);


}
