package com.neuedu.service;

import com.neuedu.entity.User;

import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/7/30 15:22
 * @Version: 1.0.0
 * @Description:
 */
public interface UserService {
    List<User> getAll();

    User findById(Integer id);

    void add(User user);

    void update(User user);

    void delete(Integer id);

    void batchDelete(List<Integer> ids);
}
