package com.neuedu.service.impl;

import com.neuedu.entity.User;
import com.neuedu.mapper.UserMapper;
import com.neuedu.service.AddressService;
import com.neuedu.service.SalesOrderService;
import com.neuedu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/5 10:12
 * @Version: 1.0.0
 * @Description: service层
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AddressService addressService;
    @Autowired
    private SalesOrderService salesOrderService;

    /**
     * @return java.util.List<com.neuedu.entity.User>
     * @create: 2019/8/5
     * @author MARK
     * @Description:
     */
    @Override
    public List<User> getAll() {
        return userMapper.selectAll();
    }

    @Override
    public User findById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public void add(User user) {
        userMapper.insert(user);
    }

    @Override
    public void update(User user) {
        userMapper.updateByPrimaryKey(user);

    }

    /**
     * @return void
     * @create: 2019/8/7
     * @author MARK
     * @Description: 删除的时候不仅用户要删除，用户存储的地址也要删除
     */
    @Override
    @Transactional
    public void delete(Integer id) {
        List<Integer> ids = new ArrayList<>();
        ids.add(id);
        addressService.deleteByUserId(ids);

        salesOrderService.deleteByUserId(ids);

        userMapper.deleteByPrimaryKey(id);
    }

    //批量删除
    //用户对应的地址也要删除
    //用户对应的订单也要删除
    @Override
    @Transactional
    public void batchDelete(List<Integer> ids) {
        addressService.deleteByUserId(ids);

        Example example = new Example(User.class);
        example.createCriteria().andIn("id", ids);
        userMapper.deleteByExample(example);

    }
}
