package com.neuedu.service.impl;

import com.neuedu.entity.ProductDetail;
import com.neuedu.entity.SalesItem;
import com.neuedu.entity.SalesOrder;
import com.neuedu.mapper.SalesItemMapper;
import com.neuedu.mapper.SalesOrderMapper;
import com.neuedu.service.ProductDetailService;
import com.neuedu.service.SalesItemService;
import com.neuedu.service.SalesOrderService;
import com.neuedu.service.UserService;
import com.neuedu.vo.SalesItemVO;
import com.neuedu.vo.SalesOrderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.swing.plaf.basic.BasicSliderUI.ScrollListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 16:32
 * @Version: 1.0.0
 * @Description:
 */
@Service
@Slf4j
public class SalesItemServiceImpl implements SalesItemService {

    @Autowired
    private SalesItemMapper salesItemMapper;
    @Autowired
    private ProductDetailService productDetailService;
    @Autowired
    private UserService userService;
    @Autowired
    private SalesOrderService salesOrderService;


    @Override
    public void add(SalesItem salesItem) {
        log.info("------salesItem" + salesItem);
        salesItemMapper.insert(salesItem);

    }

    @Override
    public void deleteById(Integer id) {
        salesItemMapper.deleteByPrimaryKey(id);
        //如果一个订单的所有订单详情都被删除那么订单也要删除 先放这吧
    }

    @Override
    public void batchDelete(List<Integer> ids) {
        Example example = new Example(SalesItem.class);
        Criteria criteria = example.createCriteria();
        criteria.andIn("id", ids);
        salesItemMapper.deleteByExample(example);
    }

    @Override
    public void deleteByOrderId(List<Integer> orderIds) {
        Example example = new Example(SalesItem.class);
        Criteria criteria = example.createCriteria();
        criteria.andIn("orderId", orderIds);
        salesItemMapper.deleteByExample(example);
    }

    @Override
    public void update(SalesItem salesItem) {
        salesItemMapper.updateByPrimaryKey(salesItem);
    }

    @Override
    public List<SalesItemVO> findAll() {
        List<SalesItemVO> listVO = new ArrayList<>();
        List<SalesItem> list = salesItemMapper.selectAll();
        for(SalesItem salesItem: list){
            SalesItemVO salesItemVO = findById(salesItem.getId());
            listVO.add(salesItemVO);
        }
        return listVO;
    }

    /**
     * @return java.util.List<com.neuedu.vo.SalesItemVO>
     * @create: 2019/8/6
     * @author MARK
     * @Description: 根据订单编号salesorder_id查询所有订单详情
     */
    @Override
    public List<SalesItemVO> findAllBySalesOrderId(Integer salesOrderId) {
        Example example = new Example(SalesOrder.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("salesOrderId", salesOrderId);
        List<SalesItem> list = salesItemMapper.selectByExample(example);

        List<SalesItemVO> listVO = new ArrayList<>();
        for(SalesItem salesItem: list){
            SalesItemVO salesItemVO = new SalesItemVO();
            salesItemVO = entityToVO(salesItem, salesItemVO);
            salesItemVO.setProductName(productDetailService.selectOne(salesItem.getProductId()).getName());
            listVO.add(salesItemVO);
        }
        return listVO;
    }

    @Override
    public SalesItemVO findById(Integer id) {
        SalesItem salesItem = salesItemMapper.selectByPrimaryKey(id);
        SalesItemVO salesItemVO = new SalesItemVO();

        salesItemVO = entityToVO(salesItem, salesItemVO);

        //根据产品id拿到产品名字
        salesItemVO.setProductName(productDetailService.selectOne(salesItem.getProductId()).getName());
        //拿到用户名
        //先拿到salesordervo对象
        SalesOrderVO salesOrderVO = salesOrderService.findBySalesOrderId(salesItem.getOrderId());
        //再从vo中取userid
        Integer userId = salesOrderVO.getUserId();
        //再从user中根据id查名字
        String username = userService.findById(userId).getUsername();
        salesItemVO.setUsername(username);
        return salesItemVO;
    }

    //根据orderid找id
    @Override
    public List<Integer> findByOrderId(Integer orderId) {
        Example example = new Example(SalesItem.class);
        example.selectProperties("id");
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("orderId", orderId);
        List<SalesItem> salesItems = salesItemMapper.selectByExample(example);
        List<Integer> ids = new ArrayList<>();
        for(SalesItem salesItem: salesItems){
            Integer id = salesItem.getId();
            ids.add(id);
        }
        return ids;
    }

    //封装一个entity转vo方法
    private SalesItemVO entityToVO(SalesItem salesItem, SalesItemVO salesItemVO){
        salesItemVO.setId(salesItem.getId());
        salesItemVO.setProductId(salesItem.getProductId());
        salesItemVO.setUnitPrice(salesItem.getUnitPrice());
        salesItemVO.setOrderId(salesItem.getOrderId());
        salesItemVO.setAddr(salesItem.getAddr());
        salesItemVO.setODate(salesItem.getODate());
        salesItemVO.setPCount(salesItem.getPCount());
        return salesItemVO;
    }


}
