package com.neuedu.service.impl;

import com.neuedu.entity.SalesItem;
import com.neuedu.entity.SalesOrder;
import com.neuedu.mapper.SalesOrderMapper;
import com.neuedu.service.ProductService;
import com.neuedu.service.SalesItemService;
import com.neuedu.service.SalesOrderService;
import com.neuedu.service.UserService;
import com.neuedu.vo.SalesItemVO;
import com.neuedu.vo.SalesOrderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 19:49
 * @Version: 1.0.0
 * @Description:
 */
@Service
@Slf4j
public class SalesOrderServiceImpl implements SalesOrderService {

    @Autowired
    private SalesOrderMapper salesOrderMapper;
    @Autowired
    private SalesItemService salesItemService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;

    @Override
    public void add(SalesOrder salesOrder) {
        salesOrderMapper.insert(salesOrder);
    }

    @Override
    public void delete(Integer salesOrderId) {
        //订单表和订单详情表有一个删除 另一个也要删除
        //在订单表删除时  跟随删除所有订单详情
        //注意删除顺序
        Example example = new Example(SalesOrder.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("salesOrderId", salesOrderId);
        salesOrderMapper.deleteByExample(example);

        List<SalesItemVO> list = salesItemService.findAllBySalesOrderId(salesOrderId);
        List<Integer> ids = new ArrayList<>();
        for(SalesItemVO salesItemVO: list){
            Integer id = salesItemVO.getId();
            ids.add(id);
        }
        salesItemService.batchDelete(ids);
    }

    @Override
    public void deleteById(Integer id) {
        salesOrderMapper.deleteByPrimaryKey(id);
    }

    //根据用户id删除订单及对应订单详情
    @Override
    public void deleteByUserId(List<Integer> userIds) {
        //订单详情删除
        List<Integer> orderIds = orderIds(userIds);
        salesItemService.deleteByOrderId(orderIds);

        //订单删除
        Example example = new Example(SalesOrder.class);
        Criteria criteria = example.createCriteria();
        criteria.andIn("userId", userIds);
        salesOrderMapper.deleteByExample(example);

        // Integer salesItemId = salesItemService.findByOrderId(orderIds);
        // salesItemService.deleteById(salesItemId);
    }



    @Override
    public void update(SalesOrder salesOrder) {
        //这里的订单修改目前只能用于修改付款状态
        salesOrderMapper.updateByPrimaryKey(salesOrder);
    }

    @Override
    public List<SalesOrderVO> findAll() {
        List<SalesOrderVO> listVO = new ArrayList<>();
        List<SalesOrder> list = salesOrderMapper.selectAll();
        for(SalesOrder salesOrder: list){
            SalesOrderVO salesOrderVO = findBySalesOrderId(salesOrder.getSalesOrderId());
            String productName = "这里直接从salesitem里传过来";
            String username = "这个也是从salesitem直接传过来";
            salesOrderVO.setProductName(productName);
            salesOrderVO.setUsername(username);
            listVO.add(salesOrderVO);
        }
        return listVO;
    }

    //根据用户id查找orderid
    @Override
    public List<Integer> orderIds(List<Integer> userIds) {
        List<Integer> orderIds = new ArrayList<>();
        Example example = new Example(SalesOrder.class);
        Criteria criteria = example.createCriteria();
        criteria.andIn("userId", userIds);
        List<SalesOrder> salesOrders = salesOrderMapper.selectByExample(example);
        for(SalesOrder salesOrder: salesOrders){
            orderIds.add(salesOrder.getSalesOrderId());
        }
        return orderIds;
    }

    @Override
    public SalesOrderVO findBySalesOrderId(Integer salesOrderId) {
        SalesOrderVO salesOrderVO = new SalesOrderVO();
        Example example = new Example(SalesOrder.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("salesOrderId", salesOrderId);
        // SalesOrder salesOrder = salesOrderMapper.selectByPrimaryKey(id);
        SalesOrder salesOrder = salesOrderMapper.selectOneByExample(example);
        log.info("------salesOrder: " + salesOrder);
        String username = "这个从salesitem直接传过来,只有一个";
        String productName = "这个也是从salesitem直接传过来,可能有多个";
        //...三表联查  //TODO 先取第一个
        salesOrderVO.setId(salesOrder.getId());
        salesOrderVO.setUserId(salesOrder.getUserId());
        salesOrderVO.setStatus(salesOrder.getStatus());
        salesOrderVO.setSalesOrderId(salesOrder.getSalesOrderId());

        salesOrderVO.setUsername(username);
        salesOrderVO.setProductName(productName);
        return salesOrderVO;
    }
}
