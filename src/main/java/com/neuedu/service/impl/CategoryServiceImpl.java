package com.neuedu.service.impl;

import com.neuedu.entity.Category;
import com.neuedu.mapper.CategoryMapper;
import com.neuedu.service.CategoryService;
import org.aspectj.weaver.patterns.ExactAnnotationFieldTypePattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/5 18:21
 * @Version: 1.0.0
 * @Description:
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public void add(Category category) {
        categoryMapper.insert(category);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        categoryMapper.deleteByPrimaryKey(id);
        List<Category> categories = findByPid(id);
        List<Integer> ids = new ArrayList<>();
        for(Category category: categories){
            ids.add(category.getId());
        }
        for(Integer integer: ids){
            deleteById(integer);
        }
    }

    /**
     * @return java.util.List<com.neuedu.entity.Category>
     * @create: 2019/8/5
     * @author MARK
     * @Description: 根据pid查找所有子类
     */
    @Override
    public List<Category> findByPid(Integer pid) {
        Example example = new Example(Category.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("pid", pid);
        List<Category> categories = categoryMapper.selectByExample(example);
        return categories;
    }

    /**
     * @return java.util.List<java.lang.Integer>
     * @create: 2019/8/9
     * @author MARK
     * @Description: 查询出所有分类等级
     */
    @Override
    public List<Integer> getGradeIds() {
        Example example = new Example(Category.class);
        example.setDistinct(true);
        example.selectProperties("grade");
        List<Category> categories = categoryMapper.selectByExample(example);
        List<Integer> grades = new ArrayList<>();
        for(Category category: categories){
            Integer grade = category.getGrade();
            grades.add(grade);
        }
        return grades;
    }

    /**
     * @return java.util.List<java.lang.String>
     * @create: 2019/8/9
     * @author MARK
     * @Description: 查询该等级所有类别id和名称
     */
    @Override
    public List<Category> findByGrade(Integer grade) {
        Example example = new Example(Category.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("grade", grade);
        example.selectProperties("id", "name");
        List<Category> categories = categoryMapper.selectByExample(example);
        return categories;
    }


    @Override
    public void update(Category category) {
        categoryMapper.updateByPrimaryKey(category);
    }

    @Override
    public List<Category> selectAll() {
        return categoryMapper.selectAll();
    }

    @Override
    public Category findById(Integer id) {
        return categoryMapper.selectByPrimaryKey(id);
    }

    // @Override
    // public void batchDelete(List<Integer> ids) {
    //
    // }


}
