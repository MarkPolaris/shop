package com.neuedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.neuedu.entity.Category;
import com.neuedu.entity.Product;
import com.neuedu.entity.ProductDetail;
import com.neuedu.mapper.ProductDetailMapper;
import com.neuedu.service.CategoryService;
import com.neuedu.service.ProductDetailService;
import com.neuedu.service.ProductService;
import com.neuedu.vo.ProductDetailVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 09:59
 * @Version: 1.0.0
 * @Description:
 */
@Service
@Slf4j
public class ProductDetailServiceImpl implements ProductDetailService {
    @Resource
    private ProductDetailMapper productDetailMapper;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductService productService;

    // @Override
    // public List<ProductDetailVO> selectAll() {
    //     List<ProductDetailVO> listVO = new ArrayList<ProductDetailVO>();
    //     List<ProductDetail> list = productDetailMapper.selectAll();
    //     for(ProductDetail productDetail: list){
    //
    //         ProductDetailVO productDetailVO = selectOne(productDetail.getId());
    //         listVO.add(productDetailVO);
    //     }
    //     return listVO;
    // }


    @Override
    public PageInfo selectAll(Integer pn, Integer pageSize) {
        List<ProductDetailVO> listVO = new ArrayList<ProductDetailVO>();

        PageHelper.startPage(pn, pageSize);
        List<ProductDetail> list = productDetailMapper.selectAll();
        PageInfo pageInfo=new PageInfo(list);


        for(ProductDetail productDetail: list){
            ProductDetailVO productDetailVO = selectOne(productDetail.getId());
            listVO.add(productDetailVO);
        }
        pageInfo.setList(listVO);
        return pageInfo;
    }

    @Override
    public ProductDetailVO selectOne(Integer id) {
        ProductDetailVO productDetailVO = new ProductDetailVO();
        ProductDetail productDetail = productDetailMapper.selectByPrimaryKey(id);
        Category category = categoryService.findById(productDetail.getCategoryId());
        Product product = productService.getById(productDetail.getProductId());
        productDetailVO.setId(productDetail.getId());
        log.info("------id: " + id);
        productDetailVO.setName(productDetail.getName());
        productDetailVO.setDescr(productDetail.getDescr());
        productDetailVO.setPDate(productDetail.getPDate());
        productDetailVO.setCategoryId(productDetail.getId());
        productDetailVO.setUpFile(productDetail.getUpFile());
        productDetailVO.setProductId(productDetail.getProductId());
        productDetailVO.setPrice(productDetail.getPrice());
        productDetailVO.setColor(productDetail.getColor());
        productDetailVO.setStock(productDetail.getStock());

        productDetailVO.setCategoryName(category.getName());
        productDetailVO.setProductName(product.getName());
        return productDetailVO;
    }

    @Override
    public void add(ProductDetail productDetail) {
        productDetailMapper.insert(productDetail);
    }

    @Override
    public void delete(Integer id) {
        productDetailMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void batchDelete(List<Integer> ids) {
        Example example = new Example(ProductDetail.class);
        example.createCriteria().andIn("id", ids);
        productDetailMapper.deleteByExample(example);
    }

    @Override
    public void update(ProductDetail productDetail) {
        productDetailMapper.updateByPrimaryKey(productDetail);
    }
}
