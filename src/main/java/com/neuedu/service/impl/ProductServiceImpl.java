package com.neuedu.service.impl;

import com.neuedu.entity.Product;
import com.neuedu.mapper.ProductMapper;
import com.neuedu.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<Product> getAll() {
        return productMapper.selectAll();
    }

    @Override
    public Product getById(Integer id) {
        return productMapper.selectByPrimaryKey(id);
    }

    @Override
    public void insert(Product product) {
        productMapper.insert(product);
    }

    @Override
    public void update(Product product) {
        productMapper.updateByPrimaryKey(product);
    }

    @Override
    public void delete(Integer id) {
        productMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void bathDelete(List<Integer> ids) {
        Example example = new Example(Product.class);
        example.createCriteria().andIn("id",ids);
        productMapper.deleteByExample(example);
    }
}
