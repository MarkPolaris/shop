package com.neuedu.service;

import com.neuedu.entity.Address;
import com.neuedu.vo.AddressVo;

import java.util.List;

public interface AddressService {
    List<AddressVo> getAll();

    AddressVo getById(Integer id);

    void insert(Address address);

    void update(Address address);

    void delete(Integer id);

    void deleteByUserId(List<Integer> userId);

    /*void bathDelete(List<Integer> ids);*/
}
