package com.neuedu.service;

import com.github.pagehelper.PageInfo;
import com.neuedu.entity.ProductDetail;
import com.neuedu.vo.ProductDetailVO;

import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 08:44
 * @Version: 1.0.0
 * @Description:
 */
public interface ProductDetailService {
    PageInfo selectAll(Integer pn, Integer pageSize);
    // List<ProductDetailVO> selectAll(Integer pn, Integer pageSize);
    //通用mapper实现多表联查,查询所有
    // List<ProductDetailVO> selectAll(Integer pn, Integer pageSize);

    ProductDetailVO selectOne(Integer id);

    void add(ProductDetail productDetail);

    void delete(Integer id);

    void batchDelete(List<Integer> ids);

    void update(ProductDetail productDetail);
}
