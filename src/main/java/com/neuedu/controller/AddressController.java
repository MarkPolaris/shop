package com.neuedu.controller;

import com.neuedu.entity.Address;
import com.neuedu.service.AddressService;
import com.neuedu.vo.AddressVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/address",produces = "application/json; charset=utf-8")
@Transactional
public class AddressController {

    @Autowired
    private AddressService addressService;

    /**
     * 查询所有
     * @return
     */
    @GetMapping
    public List<AddressVo> getAll(){
        List<AddressVo> list = addressService.getAll();
        return list;
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public AddressVo getById(@PathVariable("id") Integer id){
        AddressVo vo = addressService.getById(id);
        return vo;
    }

    /**
     * 添加操作
     * @param address
     * @return
     */
    @PostMapping
    public String insert(@RequestBody Address address){
        addressService.insert(address);
        return "ok";
    }

    /**
     * 更新操作
     * @param address
     * @return
     */
    @PutMapping
    public String update(@RequestBody Address address){
        addressService.update(address);
        return "ok";
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Integer id){
        addressService.delete(id);
        return "ok";
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    /*@DeleteMapping
    public String bathDelete(@RequestBody List<Integer> ids){
        addressService.bathDelete(ids);
        return "ok";
    }*/

    /**
     * 根据userId删除
     * @param userIds
     * @return
     */
    @DeleteMapping
    public String deleteByUserId(@RequestBody List<Integer> userIds){
        addressService.deleteByUserId(userIds);
        return "ok";
    }
}
