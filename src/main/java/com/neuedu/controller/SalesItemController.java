package com.neuedu.controller;

import com.neuedu.entity.SalesItem;
import com.neuedu.service.SalesItemService;
import com.neuedu.vo.SalesItemVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 18:02
 * @Version: 1.0.0
 * @Description:
 */
@RestController
@RequestMapping(value = "/salesItem", produces = "application/json; charset=utf-8")
public class SalesItemController {
    @Autowired
    private SalesItemService salesItemService;

    @GetMapping
    public List<SalesItemVO> getAll(){
        List<SalesItemVO> list = salesItemService.findAll();
        return list;
    }

    @GetMapping("{id}")
    public SalesItemVO getOne(@PathVariable("id") Integer id){
        return salesItemService.findById(id);
    }

    @PostMapping
    public String add(@RequestBody SalesItem salesItem){
        salesItemService.add(salesItem);
        return "ok";
    }

    @PutMapping
    public String update(SalesItem salesItem){
        salesItemService.update(salesItem);
        return "ok";
    }

    @DeleteMapping
    public String batchDelete(@RequestBody List<Integer> ids){
        salesItemService.batchDelete(ids);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String deleteById(@PathVariable("id") Integer id){
        salesItemService.deleteById(id);
        return "ok";
    }
}
