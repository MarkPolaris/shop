package com.neuedu.controller;


import com.alibaba.fastjson.JSON;
import com.neuedu.entity.User;
import com.neuedu.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/7/29 19:14
 * @version: 1.0.0
 * @Description: Restful风格
 */

@RestController
@Slf4j
@Transactional
@RequestMapping(value = "/user" ,produces = "application/json; charset=utf-8")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * @return java.lang.String
     * @create: 2019/8/1
     * @author MARK
     * @Description: 添加方法
     */
    @PostMapping
    public String addUser(@RequestBody User user){
        userService.add(user);
        return "ok";
    }

    /**
     * @return java.lang.String
     * @create: 2019/8/1
     * @author MARK
     * @Description: 更新操作
     */
    @PutMapping
    public String update(@RequestBody User user){
        userService.update(user);
        return "ok";
    }

    /**
     * @return java.util.List<com.neuedu.entity.User>
     * @create: 2019/8/1
     * @author MARK
     * @Description: 查询所有
     */
    @GetMapping
    public List<User> select(){
        List<User> list = userService.getAll();
        return list;
    }

    /**
     * @return com.neuedu.entity.User
     * @create: 2019/8/1
     * @author MARK
     * @Description: 根据id查找
     */
    @GetMapping("{id}")
    public User getById(@PathVariable("id") Integer id){
        User user = userService.findById(id);
        return user;
    }

    @DeleteMapping("{id}")
    public String deleteById(@PathVariable("id") Integer id){
        userService.delete(id);
        return "ok";
    }

    @DeleteMapping
    public String bathDelete(@RequestBody List<Integer> ids){
        // sql用in关键字批量删除 通用mapper的example对象
        userService.batchDelete(ids);
        return "ok";
    }

}
