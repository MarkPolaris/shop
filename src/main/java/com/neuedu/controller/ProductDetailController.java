package com.neuedu.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.neuedu.entity.ProductDetail;
import com.neuedu.service.ProductDetailService;
import com.neuedu.vo.ProductDetailVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/6 10:38
 * @Version: 1.0.0
 * @Description:
 */
@RestController
@Transactional
@RequestMapping(value = "/productDetail", produces="application/json;charset=UTF-8")
@CrossOrigin
@Slf4j
public class ProductDetailController {
    @Resource
    private ProductDetailService productDetailService;

    // @GetMapping
    // public List<ProductDetailVO> getAll(){
    //     List<ProductDetailVO> list = productDetailService.selectAll();
    //     return list;
    // }
    /**
     * @return com.github.pagehelper.PageInfo
     * @create: 2019/8/9
     * @author MARK
     * @Description: 获取分页信息
     */
    @GetMapping
    public PageInfo getPage(@RequestParam(value="pn", defaultValue = "1") Integer pn,
                            @RequestParam(value="pageSize") Integer pageSize){
        PageInfo<ProductDetail> pageInfo = productDetailService.selectAll(pn, pageSize);
        return pageInfo;
    }

    @GetMapping("{id}")
    public ProductDetailVO getOne(@PathVariable("id") Integer id){
        ProductDetailVO productDetailVO = productDetailService.selectOne(id);
        return productDetailVO;

    }

    @PostMapping
    public String add(@RequestBody ProductDetail productDetail){
        productDetailService.add(productDetail);
        return "ok";
    }

    @DeleteMapping(value = "{id}")
    public String delete(@PathVariable("id") Integer id){
        productDetailService.delete(id);
        return "ok";
    }

    @DeleteMapping
    public String batchDelete(@RequestBody List<Integer> ids){
        productDetailService.batchDelete(ids);
        return "ok";
    }
}
