package com.neuedu.controller;

import com.neuedu.entity.SalesOrder;
import com.neuedu.service.SalesOrderService;
import com.neuedu.vo.SalesOrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/7 11:35
 * @Version: 1.0.0
 * @Description:
 */
@RestController
@RequestMapping(value = "/salesOrder")
public class SalesOrderController {
    @Autowired
    private SalesOrderService salesOrderService;

    @GetMapping
    public List<SalesOrderVO> getAll(){
        return salesOrderService.findAll();
    }

    //没有查找一个的功能
    // @GetMapping("{id}")
    // public SalesOrder getById(@PathVariable Integer id){
    //     return salesOrderService.findById(id);
    // }

    @PostMapping
    public String add(@RequestBody SalesOrder salesOrder){
        salesOrderService.add(salesOrder);
        return "ok";
    }

    @PutMapping
    public String update(@RequestBody SalesOrder salesOrder){
        salesOrderService.update(salesOrder);
        return "ok";
    }

    @DeleteMapping("{orderId}")
    public String delete(@PathVariable("orderId") Integer orderId){
        salesOrderService.delete(orderId);
        return "ok";
    }
}
