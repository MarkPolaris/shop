package com.neuedu.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.neuedu.entity.Category;
import com.neuedu.service.CategoryService;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/8/5 19:26
 * @Version: 1.0.0
 * @Description:
 */
@RestController
@Slf4j
@RequestMapping(value = "/category", produces="application/json;charset=UTF-8")
@Transactional
//跨域
@CrossOrigin
public class CategoryController {
    @Resource
    private CategoryService categoryService;

    /**
     * @return com.github.pagehelper.PageInfo
     * @create: 2019/8/8
     * @author MARK
     * @Description: 使用pagehelper实现物理分页
     *                  pn: 当前页码
     *                  pageSize: 页面展示数据条数
     */
    @GetMapping
    public PageInfo getPage(@RequestParam(value="pn", defaultValue = "1") Integer pn,
                            @RequestParam(value="pageSize") Integer pageSize){
        PageHelper.startPage(pn, pageSize);
        List<Category> list = categoryService.selectAll();
        PageInfo<Category> pageInfo = new PageInfo<>(list);
        log.info("------getPages: " + pageInfo.getPages());
        return pageInfo;
    }

    /**
     * @return com.neuedu.entity.Category
     * @create: 2019/8/5
     * @author MARK
     * @Description: 根据pid查询,用于级联查询
     */
    @GetMapping(value = "{pid}")
    public List<Category> getByPid(@PathVariable("pid") Integer pid){
        List<Category> categorys = categoryService.findByPid(pid);
        return categorys;
    }

    @GetMapping("/grades")
    public List<Integer> getGrades(){
        List<Integer> grades = categoryService.getGradeIds();
        return grades;
    }

    @GetMapping("/grades/{grade}")
    public List<Category> getPInfo(@PathVariable("grade") Integer grade){
        List<Category> categories = categoryService.findByGrade(grade);
        return categories;
    }

    /**
     * @return java.lang.String
     * @create: 2019/8/5
     * @author MARK
     * @Description: 插入数据
     */
    @PostMapping
    public String insert(@RequestBody Category category){
        categoryService.add(category);
        return "插入成功";
    }

    /**
     * @return java.lang.String
     * @create: 2019/8/5
     * @author MARK
     * @Description: 根据id删除，级联删除
     */
    @DeleteMapping(value = "{id}")
    public String deleteById(@PathVariable("id") Integer id){
        categoryService.deleteById(id);
        return "删除成功";
    }

    /**
     * @return java.lang.String
     * @create: 2019/8/5
     * @author MARK
     * @Description: 修改信息, 注意只能改name,descr
     */
    @PutMapping
    public String update(@RequestBody Category category){
        categoryService.update(category);
        return "修改成功";
    }
}
