package com.neuedu.controller;

import com.neuedu.entity.Product;
import com.neuedu.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "/product",produces = "application/json; charset=utf-8")
@Transactional
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 查询所有
     * @return
     */
    @GetMapping
    public List<Product> getAll(){
        List<Product> list = productService.getAll();
        return list;
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public Product getById(@PathVariable("id") Integer id){
        Product product = productService.getById(id);
        return product;
    }

    /**
     * 添加功能
     * @param product
     * @return
     */
    @PostMapping
    public String insert(@RequestBody Product product){
        productService.insert(product);
        return "ok";
    }

    /**
     * 更新功能
     * @param product
     * @return
     */
    @PutMapping
    public String update(@RequestBody Product product){
        productService.update(product);
        return "ok";
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Integer id){
        productService.delete(id);
        return "ok";
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @DeleteMapping
    public String bathDelete(@RequestBody List<Integer> ids){
        productService.bathDelete(ids);
        return "ok";
    }
}
