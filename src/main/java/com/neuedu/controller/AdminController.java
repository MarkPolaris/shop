package com.neuedu.controller;

import com.neuedu.entity.Admin;
import com.neuedu.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/admin",produces = "application/json; charset=utf-8")
@Transactional
public class AdminController {

    @Autowired
    private AdminService adminService;

    /**
     * 查询所有
     * @return
     */
    @GetMapping
    public List<Admin> getAll(){
        List<Admin> admins = adminService.getAll();
        return admins;
    }

    /**
     * 根据id进行查询
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public Admin getById(@PathVariable("id") Integer id){
        Admin admin = adminService.getById(id);
        return admin;
    }

    /**
     * 添加操作
     * @param admin
     * @return
     */
    @PostMapping
    public String insert(@RequestBody Admin admin){
        adminService.add(admin);
        return "ok";
    }

    /**
     * 更新操作
     * @param admin
     * @return
     */
    @PutMapping
    public String update(@RequestBody Admin admin){
        adminService.update(admin);
        return "ok";
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public String deleteById(@PathVariable("id") Integer id){
        adminService.delete(id);
        return "ok";
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @DeleteMapping
    public String bathDelete(@RequestBody List<Integer> ids){
        adminService.batchDelete(ids);
        return "ok";
    }

}
