package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: MARK
 * @Date: 2019/8/6 08:37
 * @Version: 1.0.0
 * @Description: 产品详情表
 */
@Data
@Table(name = "t_product_detail")
public class ProductDetail {
    @Id
    private Integer id;
    private String name;
    private String descr;
    @Column(name = "p_date")
    private Date pDate;
    @Column(name = "category_id")
    private Integer categoryId;
    @Column(name = "upfile")
    private String upFile;
    @Column(name = "product_id")
    private Integer productId;
    private BigDecimal price;
    private Integer color;
    private Integer stock;
}
