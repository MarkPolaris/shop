package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author: MARK
 * @Date: 2019/8/5 18:16
 * @Version: 1.0.0
 * @Description: 分类实体类
 */
@Data
@Table(name = "t_category")
public class Category {
    @Id
    private Integer id;
    private String name;
    private String descr;
    private Integer pid;
    private Integer grade;
}
