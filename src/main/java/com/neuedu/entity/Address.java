package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "t_address")
public class Address {
    @Id
    private Integer id;
    @Column(name = "user_id")
    private Integer userId;
    private String addr;
}
