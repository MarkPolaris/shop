package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "t_product")
public class Product {
    @Id
    private Integer id;
    private String name;
    private String path;
}
