package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author: MARK
 * @Date: 2019/8/5 10:03
 * @Version: 1.0.0
 * @Description: user实体类
 */
@Data
@Table(name = "t_user")
public class User {
    @Id
    private Integer id;
    private String username;
    private String phone;
    private String password;

    @Column(name = "r_date")
    private Date rDate;
}
