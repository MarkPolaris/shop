package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author: MARK
 * @Date: 2019/8/6 19:18
 * @Version: 1.0.0
 * @Description:
 */
@Table(name = "t_salesorder")
@Data
public class SalesOrder {
    @Id
    private Integer id;
    @Column(name = "user_id")
    private Integer userId;
    private Integer status;
    @Column(name = "salesorder_id")
    private Integer salesOrderId;
}
