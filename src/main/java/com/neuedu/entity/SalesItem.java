package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: MARK
 * @Date: 2019/8/6 13:52
 * @Version: 1.0.0
 * @Description:
 */
@Data
@Table(name = "t_salesitem")
public class SalesItem {
    @Id
    private Integer id;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "unit_price")
    private BigDecimal unitPrice;
    @Column(name = "p_count")
    private Integer pCount;
    @Column(name = "salesorder_id")
    private Integer orderId;
    private String addr;
    @Column(name = "o_date")
    private Date oDate;
}
