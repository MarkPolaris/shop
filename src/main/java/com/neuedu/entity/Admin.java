package com.neuedu.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "t_admin")
public class Admin {
    @Id
    private Integer id;
    @Column(name = "a_name")
    private String aName;
    @Column(name = "a_pwd")
    private String aPwd;
    private Integer role;
}
