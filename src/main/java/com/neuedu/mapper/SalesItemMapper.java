package com.neuedu.mapper;

import com.neuedu.entity.SalesItem;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;


/**
 * @Author: MARK
 * @Date: 2019/8/6 16:26
 * @Version: 1.0.0
 * @Description:
 */
@Repository
public interface SalesItemMapper extends Mapper<SalesItem> {
}
