package com.neuedu.mapper;

import com.neuedu.entity.ProductDetail;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: MARK
 * @Date: 2019/8/6 08:43
 * @Version: 1.0.0
 * @Description:
 */
@Repository
public interface ProductDetailMapper extends Mapper<ProductDetail> {
}
