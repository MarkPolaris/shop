package com.neuedu.mapper;

import com.neuedu.entity.SalesOrder;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: MARK
 * @Date: 2019/8/6 19:41
 * @Version: 1.0.0
 * @Description:
 */
@Repository
public interface SalesOrderMapper extends Mapper<SalesOrder> {
}
