package com.neuedu.mapper;

import com.neuedu.entity.User;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: MARK
 * @Date: 2019/8/5 10:03
 * @Version: 1.0.0
 * @Description: 通用mapper
 */
@Repository
public interface UserMapper extends Mapper<User> {
}
