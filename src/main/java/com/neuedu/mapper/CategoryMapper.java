package com.neuedu.mapper;


import com.neuedu.entity.Category;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Author: MARK
 * @Date: 2019/8/5 18:19
 * @Version: 1.0.0
 * @Description:
 */
@Repository
public interface CategoryMapper extends Mapper<Category> {
}
