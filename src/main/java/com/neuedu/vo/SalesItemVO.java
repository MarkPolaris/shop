package com.neuedu.vo;

import lombok.Data;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: MARK
 * @Date: 2019/8/6 14:19
 * @Version: 1.0.0
 * @Description:
 */
@Data
public class SalesItemVO {
    private Integer id;
    private Integer productId;
    private BigDecimal unitPrice;
    private Integer pCount;
    private Integer orderId;
    private String addr;
    private Date oDate;

    private String productName;
    private String username;

}
