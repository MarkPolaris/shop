package com.neuedu.vo;

import lombok.Data;

import javax.persistence.Column;

/**
 * @Author: MARK
 * @Date: 2019/8/6 20:17
 * @Version: 1.0.0
 * @Description:
 */
@Data
public class SalesOrderVO {
    private Integer id;
    private Integer userId;
    private Integer status;
    private Integer salesOrderId;

    private String username;
    private String productName;
}
