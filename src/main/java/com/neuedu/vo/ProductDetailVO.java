package com.neuedu.vo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: MARK
 * @Date: 2019/8/6 09:42
 * @Version: 1.0.0
 * @Description:
 */
@Data
public class ProductDetailVO {
    private Integer id;
    private String name;
    private String descr;
    private Date pDate;
    private Integer categoryId;
    private String upFile;
    private Integer productId;
    private BigDecimal price;
    private Integer color;
    private Integer stock;

    private String categoryName;
    private String productName;
}
