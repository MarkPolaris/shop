package com.neuedu.vo;

import lombok.Data;

@Data
public class AddressVo {
    private Integer id;
    private Integer userId;
    private String addr;
    private String username;
}
