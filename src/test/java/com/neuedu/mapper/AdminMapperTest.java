package com.neuedu.mapper;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.neuedu.AppTest;
import com.neuedu.entity.Admin;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class AdminMapperTest extends AppTest {
    @Resource
    private AdminMapper adminMapper;
    @Test
    public void test() {
        PageHelper.startPage(1,2);
        List<Admin> admins = adminMapper.selectAll();
        PageInfo<Admin> pageInfo=new PageInfo<>(admins);
        log.info(JSONArray.toJSONString(pageInfo));

    }

}