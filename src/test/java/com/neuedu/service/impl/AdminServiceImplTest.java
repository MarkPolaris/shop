package com.neuedu.service.impl;

import com.neuedu.AppTest;
import com.neuedu.entity.Admin;
import com.neuedu.service.AdminService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;


public class AdminServiceImplTest extends AppTest {

    @Autowired
    private AdminService adminService;

    @Test
    public void getAll() {
        adminService.getAll();
    }

    @Test
    public void add() {
        Admin admin = new Admin();

        admin.setAName("admin");
        admin.setAPwd("123456");
        admin.setRole(1);
        adminService.add(admin);
    }

    @Test
    public void update() {
        Admin admin = adminService.getById(1);
        admin.setAPwd("000000");
        adminService.update(admin);
    }

    @Test
    public void delete() {
        adminService.delete(2);
    }
}