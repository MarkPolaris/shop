package com.neuedu.service.impl;

import com.neuedu.AppTest;
import com.neuedu.entity.Address;
import com.neuedu.service.AddressService;
import com.neuedu.vo.AddressVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
@Slf4j
public class AddressServiceImplTest extends AppTest {

    @Autowired
    private AddressService addressService;

    @Test
    public void getAll() {
        List<AddressVo> voList = addressService.getAll();
        log.info(voList.toString());
    }

    @Test
    public void getById() {
        AddressVo vo = addressService.getById(18);
        log.info(vo.toString());
    }

    @Test
    public void insert() {
        Address address = new Address();
        address.setUserId(3);
        address.setAddr("广州武汉");
        addressService.insert(address);

    }

    @Test
    public void update() {
        Address address =new Address();
        address.setId(2);
        address.setUserId(2);
        address.setAddr("天津");
        addressService.update(address);
    }

    @Test
    public void delete() {
        addressService.delete(5);
    }

    /*@Test
    public void bathDelete() {
        List<Integer> ids = new ArrayList<>();
        ids.add(4);
        ids.add(3);
        addressService.bathDelete(ids);
    }*/
    @Test
    public void deleteByUserId(){
        List<Integer> userId = new ArrayList<>();
        userId.add(1);
        userId.add(3);
        addressService.deleteByUserId(userId);
    }
}