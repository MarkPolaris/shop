package com.neuedu.service.impl;

import com.neuedu.AppTest;
import com.neuedu.entity.SalesItem;
import com.neuedu.service.SalesItemService;
import com.neuedu.vo.SalesItemVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/8/6 16:37
 * @Version: 1.0.0
 * @Description:
 */
@Slf4j
public class SalesItemServiceImplTest extends AppTest {
    @Autowired
    private SalesItemService salesItemService;

    @Test
    public void add() {
        SalesItem salesItem = new SalesItem();
        salesItem.setProductId(1);
        salesItem.setUnitPrice(new BigDecimal(99.0));

        salesItemService.add(salesItem);
    }

    @Test
    public void deleteById() {
        salesItemService.deleteById(1);
    }

    @Test
    public void batchDelete() {
        List<Integer> ids = new ArrayList<>();
        ids.add(3);
        ids.add(2);
        salesItemService.batchDelete(ids);
    }

    @Test
    public void update() {
        SalesItem salesItem = new SalesItem();
        salesItem.setId(2);
        salesItem.setUnitPrice(new BigDecimal(299));
        salesItemService.update(salesItem);
    }

    @Test
    public void findAll() {
        List<SalesItemVO> list = salesItemService.findAll();
        log.info(list.toString());
    }

    @Test
    public void findByOrderId() {
        List<Integer> ids = salesItemService.findByOrderId(1001);
        log.info("------" + ids);

    }
}