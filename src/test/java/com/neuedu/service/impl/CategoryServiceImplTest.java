package com.neuedu.service.impl;

import com.neuedu.AppTest;
import com.neuedu.entity.Category;
import com.neuedu.service.CategoryService;
import com.neuedu.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/8/5 18:57
 * @Version: 1.0.0
 * @Description:
 */
@Slf4j
public class CategoryServiceImplTest extends AppTest {
    @Autowired
    private CategoryService categoryService;

    @Test
    public void add() {
        Category category = new Category();
        category.setName("分类测试1");
        category.setDescr("描述balabala");
        categoryService.add(category);
    }

    @Test
    public void deleteById() {
        categoryService.deleteById(1832);
    }

    @Test
    public void findByPid() {
        List<Category> list = categoryService.findByPid(1828);
        log.info(list.toString());
    }

    @Test
    public void update() {
    }

    @Test
    public void selectAll() {
    }

    @Test
    public void findById() {
        Category category = categoryService.findById(1827);
        log.info(category.toString());
    }
}