package com.neuedu.service.impl;

import com.neuedu.AppTest;
import com.neuedu.entity.User;
import com.neuedu.mapper.UserMapper;
import com.neuedu.service.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/8/5 10:48
 * @Version: 1.0.0
 * @Description:
 */
public class UserServiceImplTest extends AppTest {
    @Autowired
    private UserService userService;

    @Test
    public void getAll() {
        List<User> list = userService.getAll();
        System.out.println(list);
    }

    @Test
    public void findById() {
        User user = userService.findById(1);
        System.out.println(user);
    }

    @Test
    public void add() {
        User user = new User();
        user.setUsername("mm");
        user.setPassword("123");
        user.setPhone("10000");
        user.setRDate(new Date());
        userService.add(user);
    }

    @Test
    public void update() {
        User user = new User();
        user.setId(3);
        user.setUsername("ww");
        userService.update(user);
    }

    @Test
    public void delete() {
        userService.delete(1);
    }

    @Test
    public void batchDelete(){
        List<Integer> ids = new ArrayList<>();
        ids.add(5);
        ids.add(4);
        userService.batchDelete(ids);
    }
}