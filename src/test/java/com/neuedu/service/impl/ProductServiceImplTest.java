package com.neuedu.service.impl;

import com.neuedu.AppTest;
import com.neuedu.entity.Product;
import com.neuedu.service.ProductService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProductServiceImplTest extends AppTest {

    @Autowired
    private ProductService productService;

    @Test
    public void getAll() {
        List<Product> list = productService.getAll();
        for (Product product: list) {
            System.out.println(product);
        }
    }

    @Test
    public void getById() {
        Product product = productService.getById(1);
        System.out.println(product);
    }

    @Test
    public void insert() {
        Product product = new Product();
        product.setName("产品05");
        product.setPath("asdfgqwert");
        productService.insert(product);

    }

    @Test
    public void update() {
        Product product = productService.getById(2);
        product.setName("服装产品");
        productService.update(product);
    }

    @Test
    public void delete() {
        productService.delete(4);
    }

    @Test
    public void bathDelete() {
        List<Integer> ids = new ArrayList<>();
        ids.add(6);
        ids.add(7);
        productService.bathDelete(ids);
    }
}