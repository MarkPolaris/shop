package com.neuedu.service.impl;

import com.neuedu.AppTest;
import com.neuedu.entity.ProductDetail;
import com.neuedu.mapper.ProductDetailMapper;
import com.neuedu.service.ProductDetailService;
import com.neuedu.vo.ProductDetailVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/8/6 10:30
 * @Version: 1.0.0
 * @Description:
 */
@Slf4j
public class ProductDetailServiceImplTest extends AppTest {
    @Autowired
    private ProductDetailService productDetailService;

    @Test
    public void selectAll() {
        List<ProductDetailVO> list = productDetailService.selectAll();
        log.info(list.toString());
    }

    @Test
    public void selectOne() {
        ProductDetailVO productDetailVO = productDetailService.selectOne(1);
        log.info(productDetailVO.toString());
    }

    @Test
    public void add() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setName("产品4");
        productDetailService.add(productDetail);
    }

    @Test
    public void delete() {
        productDetailService.delete(3);
    }

    @Test
    public void batchDelete() {
        List<Integer> ids = new ArrayList<>();
        ids.add(5);
        ids.add(4);
        productDetailService.batchDelete(ids);
    }

    @Test
    public void update() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setId(2);
        productDetail.setDescr("版内发");
        productDetailService.update(productDetail);
    }
}